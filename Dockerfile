FROM node:8

RUN mkdir /app

WORKDIR /app

COPY package.json /app/

RUN npm install --production

COPY . /app

EXPOSE 443

CMD ["npm", "start"]